"use client";
"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FormContext = void 0;
const react_1 = __importStar(require("react"));
const Form_module_scss_1 = __importDefault(require("./Form.module.scss"));
const classnames_1 = __importDefault(require("classnames"));
const jsel_1 = require("@dexodus/jsel");
const Event_1 = require("@dexodus/jsel/src/Event/Event");
exports.FormContext = (0, react_1.createContext)(undefined);
const isObjectsEqual = (object1, object2) => {
    return JSON.stringify(object1) === JSON.stringify(object2);
};
const Form = ({ className, children, data, setData }) => {
    const [_, setCounter] = (0, react_1.useState)(0);
    const prevDataRef = (0, react_1.useRef)(data);
    const jselRef = (0, react_1.useRef)();
    const reRender = () => {
        setCounter(counter => counter + 1);
    };
    if (!jselRef.current) {
        jselRef.current = new jsel_1.Jsel(new jsel_1.JselContext({ data: { ...prevDataRef.current } }));
    }
    (0, react_1.useEffect)(() => {
        jselRef.current?.addEventListener(Event_1.EventType.ASSIGN, event => {
            if (isObjectsEqual(prevDataRef.current, event.globalScope.data)) {
                return;
            }
            setData({ ...event.globalScope.data });
            prevDataRef.current = JSON.parse(JSON.stringify(event.globalScope.data));
        });
    }, []);
    return (react_1.default.createElement("div", { className: (0, classnames_1.default)(Form_module_scss_1.default.form, className) },
        react_1.default.createElement(exports.FormContext.Provider, { value: {
                jselRef: jselRef,
                reRender,
                context: "data",
                options: {
                    rootProperty: "data",
                    enableValidation: true,
                },
            } }, children)));
};
exports.default = Form;
//# sourceMappingURL=Form.js.map