"use client";

import React, {createContext, useEffect, useRef, useState} from "react";
import styles from "./Form.module.scss";
import classNames from "classnames";
import {Jsel, JselContext} from "@dexodus/jsel";
import {EventType} from "@dexodus/jsel/src/Event/Event";

export type FormData = { [property: string]: any | FormData };

interface FormProps {
    className?: string;
    children?: React.ReactNode;
    data: FormData;
    setData: React.Dispatch<React.SetStateAction<FormData>>;
}


export interface FormContextValueOptions {
    rootProperty: string;
    enableValidation: boolean;
}

export interface FormContextValue {
    jselRef: React.MutableRefObject<Jsel>;
    reRender: () => void;
    context: string;
    options: FormContextValueOptions;
}

export const FormContext = createContext<FormContextValue | undefined>(undefined);

const isObjectsEqual = (object1: Object, object2: Object) => {
    return JSON.stringify(object1) === JSON.stringify(object2);
};

const Form: React.FC<FormProps> = ({className, children, data, setData}) => {
    const [_, setCounter] = useState<number>(0);
    const prevDataRef = useRef<FormData>(data);
    const jselRef = useRef<Jsel>();

    const reRender = () => {
        setCounter(counter => counter + 1);
    };

    if (!jselRef.current) {
        jselRef.current = new Jsel(new JselContext({data: {...prevDataRef.current}}));
    }

    useEffect(() => {
        jselRef.current?.addEventListener(EventType.ASSIGN, event => {
            if (isObjectsEqual(prevDataRef.current, event.globalScope.data)) {
                return;
            }

            setData({...event.globalScope.data});
            prevDataRef.current = JSON.parse(JSON.stringify(event.globalScope.data));
        });
    }, []);

    return (
        <div className={classNames(styles.form, className)}>
            <FormContext.Provider value={{
                jselRef: jselRef as React.MutableRefObject<Jsel>,
                reRender,
                context: "data",
                options: {
                    rootProperty: "data",
                    enableValidation: true,
                },
            }}>
                {children}
            </FormContext.Provider>
        </div>
    );
};

export default Form;
