import { FormContextValue } from "../Form";
declare const useFormContext: (property: string, defaultValue: any) => FormContextValue;
export default useFormContext;
