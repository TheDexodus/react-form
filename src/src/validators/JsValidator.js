"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class JsValidator {
    constructor(validateFunction) {
        this.validateFunction = validateFunction;
    }
    validate(data, value) {
        return this.validateFunction(data, value);
    }
}
exports.default = JsValidator;
//# sourceMappingURL=JsValidator.js.map