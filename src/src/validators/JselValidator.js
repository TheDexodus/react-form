"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const jsel_1 = require("@dexodus/jsel");
class JselValidator {
    constructor(validationCode, errorMessage) {
        this.validationCode = validationCode;
        this.errorMessage = errorMessage;
        this.scope = {};
        this.jsel = new jsel_1.Jsel(new jsel_1.JselContext(this.scope));
    }
    validate(data, value) {
        this.scope.data = data;
        this.scope.currentValue = value;
        if (this.jsel.exec(this.validationCode)) {
            return true;
        }
        return this.errorMessage;
    }
}
exports.default = JselValidator;
//# sourceMappingURL=JselValidator.js.map