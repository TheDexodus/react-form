import React from "react";
interface ValidationErrorsProps {
    validationErrors: string[];
    className?: {
        validationErrorsGroup?: string;
        validationError?: string;
    };
}
declare const ValidationErrors: React.FC<ValidationErrorsProps>;
export default ValidationErrors;
