"use client";
"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const Form_1 = require("../../Form");
const ArrayField_module_scss_1 = __importDefault(require("./ArrayField.module.scss"));
const useFormContext_1 = __importDefault(require("../../hooks/useFormContext"));
const ObjectField_1 = __importDefault(require("../../fields/ObjectField"));
const classnames_1 = __importDefault(require("classnames"));
const ValidationErrors_1 = __importDefault(require("../../ValidationErrors"));
const useValidationErrors_1 = __importDefault(require("../../hooks/useValidationErrors"));
const ArrayField = ({ className, property, children, label, validators }) => {
    const { jselRef, reRender, context: fieldPath, options } = (0, useFormContext_1.default)(property, []);
    const validationErrors = (0, useValidationErrors_1.default)(options, jselRef, fieldPath, validators);
    const value = jselRef.current?.exec(fieldPath);
    const addChild = () => {
        jselRef.current?.assign(`${fieldPath}[${value.length}]`, {});
        reRender();
    };
    const removeChild = (index) => {
        const arrayValue = jselRef.current?.exec(fieldPath);
        arrayValue.splice(index, 1);
        jselRef.current?.assign(`${fieldPath}`, arrayValue);
        reRender();
    };
    return (0, react_1.useMemo)(() => (react_1.default.createElement(react_1.default.Fragment, null, Array.isArray(value) && (react_1.default.createElement("div", { className: (0, classnames_1.default)(ArrayField_module_scss_1.default.arrayField, className?.formGroup) },
        react_1.default.createElement("div", { className: className?.label },
            label ?? property,
            react_1.default.createElement("button", { className: className?.addChildButton, onClick: () => addChild() }, "+")),
        react_1.default.createElement("div", { className: (0, classnames_1.default)(ArrayField_module_scss_1.default.arrayField__children, className?.input) },
            react_1.default.createElement(Form_1.FormContext.Provider, { value: { jselRef, reRender, context: fieldPath, options } }, value.map((_, index) => (react_1.default.createElement("div", { key: index },
                react_1.default.createElement("button", { className: className?.removeChildButton, onClick: () => removeChild(index) }, "-"),
                react_1.default.createElement(ObjectField_1.default, { property: `[${index}]` }, children)))))),
        react_1.default.createElement(ValidationErrors_1.default, { validationErrors: validationErrors, className: { validationErrorsGroup: className?.validationErrorsGroup, validationError: className?.validationError } }))))), [JSON.stringify(value), validationErrors]);
};
exports.default = ArrayField;
//# sourceMappingURL=ArrayField.js.map