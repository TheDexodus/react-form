import React from "react";
export interface FieldComponentProps {
    value: any;
    onChange: (value: any) => void;
    className?: string;
}
type FieldComponent<Props extends FieldComponentProps> = React.FC<Props>;
export default FieldComponent;
