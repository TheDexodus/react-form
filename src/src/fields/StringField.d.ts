import FieldComponent, { FieldComponentProps } from "../fields/FieldComponent";
interface StringFieldProps extends FieldComponentProps {
}
declare const StringField: FieldComponent<StringFieldProps>;
export default StringField;
