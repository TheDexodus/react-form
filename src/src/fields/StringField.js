"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const StringField = ({ value, onChange, className }) => {
    return (react_1.default.createElement("input", { className: className, type: "text", value: value, onChange: event => onChange(event.target.value) }));
};
exports.default = StringField;
//# sourceMappingURL=StringField.js.map