"use client";
"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const useFormContext_1 = __importDefault(require("../hooks/useFormContext"));
const useValidationErrors_1 = __importDefault(require("../hooks/useValidationErrors"));
const ValidationErrors_1 = __importDefault(require("../ValidationErrors"));
const Field = ({ className, property, component, label, validators }) => {
    const { jselRef, reRender, context: fieldPath, options } = (0, useFormContext_1.default)(property, '');
    const validationErrors = (0, useValidationErrors_1.default)(options, jselRef, fieldPath, validators);
    const value = jselRef.current?.exec(fieldPath);
    const Component = component;
    return (0, react_1.useMemo)(() => (react_1.default.createElement(react_1.default.Fragment, null, value !== undefined && (react_1.default.createElement("div", { className: className?.formGroup },
        react_1.default.createElement("div", { className: className?.label }, label ?? property),
        react_1.default.createElement(Component, { value: value, onChange: value => {
                jselRef.current.assign(fieldPath, value);
                reRender();
            }, className: className?.input }),
        react_1.default.createElement(ValidationErrors_1.default, { validationErrors: validationErrors, className: { validationErrorsGroup: className?.validationErrorsGroup, validationError: className?.validationError } }))))), [JSON.stringify(value), validationErrors]);
};
exports.default = Field;
//# sourceMappingURL=Field.js.map