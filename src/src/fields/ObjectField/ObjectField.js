"use client";
"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const Form_1 = require("../../Form");
const ObjectField_module_scss_1 = __importDefault(require("./ObjectField.module.scss"));
const useFormContext_1 = __importDefault(require("../../hooks/useFormContext"));
const useValidationErrors_1 = __importDefault(require("../../hooks/useValidationErrors"));
const ValidationErrors_1 = __importDefault(require("../../ValidationErrors"));
const classnames_1 = __importDefault(require("classnames"));
const ObjectField = ({ className, property, children, label, validators }) => {
    const { jselRef, reRender, context: fieldPath, options } = (0, useFormContext_1.default)(property, {});
    const validationErrors = (0, useValidationErrors_1.default)(options, jselRef, fieldPath, validators);
    const value = jselRef.current?.exec(fieldPath);
    return (0, react_1.useMemo)(() => (react_1.default.createElement(react_1.default.Fragment, null, value !== undefined && (react_1.default.createElement("div", { className: (0, classnames_1.default)(ObjectField_module_scss_1.default.objectField, className?.formGroup) },
        react_1.default.createElement("div", { className: className?.label }, label ?? property),
        react_1.default.createElement("div", { className: (0, classnames_1.default)(ObjectField_module_scss_1.default.objectField__children, className?.input) },
            react_1.default.createElement(Form_1.FormContext.Provider, { value: { jselRef, reRender, context: fieldPath, options } }, children)),
        react_1.default.createElement(ValidationErrors_1.default, { validationErrors: validationErrors, className: { validationErrorsGroup: className?.validationErrorsGroup, validationError: className?.validationError } }))))), [JSON.stringify(value), validationErrors]);
};
exports.default = ObjectField;
//# sourceMappingURL=ObjectField.js.map