import React from "react";
import GeneralField, { GeneralFieldProps } from "../../fields/GeneralField";
interface ObjectFieldProps extends GeneralFieldProps {
    children?: React.ReactNode;
}
declare const ObjectField: GeneralField<ObjectFieldProps>;
export default ObjectField;
