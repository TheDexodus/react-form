"use client";

import React, {useMemo} from "react";
import {FormContext} from "../../Form";
import styles from "./ArrayField.module.scss"
import useFormContext from "../../hooks/useFormContext";
import ObjectField from "../../fields/ObjectField";
import GeneralField, {GeneralFieldClassName, GeneralFieldProps} from "../../fields/GeneralField";
import classNames from "classnames";
import ValidationErrors from "../../ValidationErrors";
import useValidationErrors from "../../hooks/useValidationErrors";

interface ArrayFieldClassName extends GeneralFieldClassName {
    addChildButton?: string;
    removeChildButton?: string;
}

interface ArrayFieldProps extends GeneralFieldProps {
    children?: React.ReactNode;
    className?: ArrayFieldClassName;
}

const ArrayField: GeneralField<ArrayFieldProps> = ({className, property, children, label, validators}) => {
    const {jselRef, reRender, context: fieldPath, options} = useFormContext(property, []);
    const validationErrors = useValidationErrors(options, jselRef, fieldPath, validators);
    const value = jselRef.current?.exec(fieldPath);

    const addChild = () => {
        jselRef.current?.assign(`${fieldPath}[${value.length}]`, {})
        reRender();
    }

    const removeChild = (index: number) => {
        const arrayValue = jselRef.current?.exec(fieldPath) as Array<Object>;
        arrayValue.splice(index, 1);
        jselRef.current?.assign(`${fieldPath}`, arrayValue);
        reRender();
    }

    return useMemo(() => (
        <>
            {Array.isArray(value) && (
                <div className={classNames(styles.arrayField, className?.formGroup)}>
                    <div className={className?.label}>
                        {label ?? property}
                        <button className={className?.addChildButton} onClick={() => addChild()}>+</button>
                    </div>
                    <div className={classNames(styles.arrayField__children, className?.input)}>
                        <FormContext.Provider value={{jselRef, reRender, context: fieldPath, options}}>
                            {value.map((_, index) => (
                                <div key={index}>
                                    <button className={className?.removeChildButton} onClick={() => removeChild(index)}>
                                        -
                                    </button>
                                    <ObjectField property={`[${index}]`}>
                                        {children}
                                    </ObjectField>
                                </div>
                            ))}
                        </FormContext.Provider>
                    </div>
                    <ValidationErrors
                        validationErrors={validationErrors}
                        className={{validationErrorsGroup: className?.validationErrorsGroup, validationError: className?.validationError}}
                    />
                </div>
            )}
        </>
    ), [JSON.stringify(value), validationErrors]);
}

export default ArrayField;
