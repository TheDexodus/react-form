import React from "react";
import GeneralField, { GeneralFieldClassName, GeneralFieldProps } from "../../fields/GeneralField";
interface ArrayFieldClassName extends GeneralFieldClassName {
    addChildButton?: string;
    removeChildButton?: string;
}
interface ArrayFieldProps extends GeneralFieldProps {
    children?: React.ReactNode;
    className?: ArrayFieldClassName;
}
declare const ArrayField: GeneralField<ArrayFieldProps>;
export default ArrayField;
