"use client";

import React, {useMemo} from "react";
import FieldComponent, {FieldComponentProps} from "../fields/FieldComponent";
import useFormContext from "../hooks/useFormContext";
import GeneralField, {GeneralFieldProps} from "../fields/GeneralField";
import useValidationErrors from "../hooks/useValidationErrors";
import ValidationErrors from "../ValidationErrors";

interface FieldProps extends GeneralFieldProps {
    component: FieldComponent<any>;
    componentProps?: {[propertyName: string]: any}
}

const Field: GeneralField<FieldProps> = ({className, property, component, label, validators, componentProps}) => {
    const {jselRef, reRender, context: fieldPath, options} = useFormContext(property, '');
    const validationErrors = useValidationErrors(options, jselRef, fieldPath, validators);
    const value = jselRef.current?.exec(fieldPath);
    const Component = component;

    return useMemo(() => (
        <>
            {value !== undefined && (
                <div className={className?.formGroup}>
                    <div className={className?.label}>{label ?? property}</div>
                    <Component
                        {...componentProps}
                        value={value}
                        onChange={value => {
                            jselRef.current.assign(fieldPath, value)
                            reRender();
                        }}
                        className={className?.input}
                    />
                    <ValidationErrors
                        validationErrors={validationErrors}
                        className={{validationErrorsGroup: className?.validationErrorsGroup, validationError: className?.validationError}}
                    />
                </div>
            )}
        </>
    ), [JSON.stringify(value), validationErrors]);
};

export default Field;
