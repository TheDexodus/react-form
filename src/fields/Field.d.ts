import FieldComponent, { FieldComponentProps } from "../fields/FieldComponent";
import GeneralField, { GeneralFieldProps } from "../fields/GeneralField";
interface FieldProps extends GeneralFieldProps {
    component: FieldComponent<FieldComponentProps>;
    componentProps?: {
        [propertyName: string]: any;
    };
}
declare const Field: GeneralField<FieldProps>;
export default Field;
