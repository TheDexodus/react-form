"use client";

import React, {useMemo} from "react";
import {FormContext} from "../../Form";
import styles from "./ObjectField.module.scss"
import useFormContext from "../../hooks/useFormContext";
import GeneralField, {GeneralFieldProps} from "../../fields/GeneralField";
import useValidationErrors from "../../hooks/useValidationErrors";
import ValidationErrors from "../../ValidationErrors";
import classNames from "classnames";

interface ObjectFieldProps extends GeneralFieldProps {
    children?: React.ReactNode;
}

const ObjectField: GeneralField<ObjectFieldProps> = ({className, property, children, label, validators}) => {
    const {jselRef, reRender, context: fieldPath, options} = useFormContext(property, {});
    const validationErrors = useValidationErrors(options, jselRef, fieldPath, validators);
    const value = jselRef.current?.exec(fieldPath);

    return useMemo(() => (
        <>
            {value !== undefined && (
                <div className={classNames(styles.objectField, className?.formGroup)}>
                    <div className={className?.label}>{label ?? property}</div>
                    <div className={classNames(styles.objectField__children, className?.input)}>
                        <FormContext.Provider value={{jselRef, reRender, context: fieldPath, options}}>
                            {children}
                        </FormContext.Provider>
                    </div>
                    <ValidationErrors
                        validationErrors={validationErrors}
                        className={{validationErrorsGroup: className?.validationErrorsGroup, validationError: className?.validationError}}
                    />
                </div>
            )}
        </>
    ), [JSON.stringify(value), validationErrors]);
}

export default ObjectField;
