import React from "react";
import ValidatorInterface from "../validators/ValidatorInterface";
export interface GeneralFieldClassName {
    formGroup?: string;
    label?: string;
    input?: string;
    validationErrorsGroup?: string;
    validationError?: string;
}
export interface GeneralFieldProps {
    property: string;
    label?: React.ReactNode;
    validators?: Array<ValidatorInterface>;
    className?: GeneralFieldClassName;
}
type GeneralField<Props extends GeneralFieldProps> = React.FC<Props>;
export default GeneralField;
