import ValidatorInterface from "../validators/ValidatorInterface";
export default class JselValidator implements ValidatorInterface {
    private validationCode;
    private errorMessage;
    private jsel;
    private scope;
    constructor(validationCode: string, errorMessage: string);
    validate(data: any, value: any): true | string;
}
