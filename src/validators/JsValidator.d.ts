import ValidatorInterface from "../validators/ValidatorInterface";
export default class JsValidator implements ValidatorInterface {
    private validateFunction;
    constructor(validateFunction: (data: any, value: any) => Promise<true | string> | true | string);
    validate(data: any, value: any): Promise<true | string> | true | string;
}
