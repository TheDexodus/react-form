import ValidatorInterface from "../validators/ValidatorInterface";
declare const applyValidators: (validators: ValidatorInterface[], data: any, value: any) => Promise<string[]>;
export default applyValidators;
