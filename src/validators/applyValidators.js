"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const applyValidators = async (validators, data, value) => {
    const results = validators.map(validator => validator.validate(data, value));
    const pureResults = [];
    const promiseResults = [];
    for (const result of results) {
        if (result instanceof Promise) {
            promiseResults.push(result);
        }
        else {
            pureResults.push(result);
        }
    }
    const errors = [...pureResults, ...await Promise.all(promiseResults)];
    return errors.filter(error => error !== true);
};
exports.default = applyValidators;
//# sourceMappingURL=applyValidators.js.map