"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = require("react");
const Form_1 = require("../Form");
const useFormContext = (property, defaultValue) => {
    const formContextValue = (0, react_1.useContext)(Form_1.FormContext);
    if (!formContextValue) {
        throw new Error("This field need use in Form component");
    }
    const { jselRef, reRender, context, options } = formContextValue;
    const fieldPath = property.startsWith("[") && property.endsWith("]")
        ? `${context}${property}`
        : `${context}.${property}`;
    (0, react_1.useEffect)(() => {
        if (jselRef.current?.exec(fieldPath) === undefined) {
            jselRef.current?.assign(fieldPath, defaultValue);
            reRender();
        }
    }, []);
    return { jselRef, reRender, context: fieldPath, options };
};
exports.default = useFormContext;
//# sourceMappingURL=useFormContext.js.map