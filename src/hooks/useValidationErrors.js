"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = require("react");
const applyValidators_1 = __importDefault(require("../validators/applyValidators"));
const useValidationErrors = (options, jselRef, fieldPath, validators) => {
    const [validationErrors, setValidationErrors] = (0, react_1.useState)([]);
    (0, react_1.useEffect)(() => {
        const value = jselRef.current?.exec(fieldPath);
        if (options.enableValidation && value !== undefined) {
            const data = jselRef.current.exec(options.rootProperty);
            (async () => {
                const errors = await (0, applyValidators_1.default)(validators ?? [], data, value);
                setValidationErrors(errors);
            })();
        }
        else {
            setValidationErrors([]);
        }
    }, [options.enableValidation, jselRef.current?.exec(fieldPath) === undefined, JSON.stringify(jselRef.current?.exec(fieldPath))]);
    return validationErrors;
};
exports.default = useValidationErrors;
//# sourceMappingURL=useValidationErrors.js.map