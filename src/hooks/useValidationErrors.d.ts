import React from "react";
import { FormContextValueOptions } from "../Form";
import { Jsel } from "@dexodus/jsel";
import ValidatorInterface from "../validators/ValidatorInterface";
declare const useValidationErrors: (options: FormContextValueOptions, jselRef: React.MutableRefObject<Jsel>, fieldPath: string, validators: Array<ValidatorInterface> | undefined) => string[];
export default useValidationErrors;
