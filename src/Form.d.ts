import React from "react";
import { Jsel } from "@dexodus/jsel";
export type FormData = {
    [property: string]: any | FormData;
};
interface FormProps {
    className?: string;
    children?: React.ReactNode;
    data: FormData;
    setData: React.Dispatch<React.SetStateAction<FormData>>;
}
export interface FormContextValueOptions {
    rootProperty: string;
    enableValidation: boolean;
}
export interface FormContextValue {
    jselRef: React.MutableRefObject<Jsel>;
    reRender: () => void;
    context: string;
    options: FormContextValueOptions;
}
export declare const FormContext: React.Context<FormContextValue>;
declare const Form: React.FC<FormProps>;
export default Form;
