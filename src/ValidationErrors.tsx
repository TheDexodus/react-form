import React from "react";

interface ValidationErrorsProps {
    validationErrors: string[];
    className?: {
        validationErrorsGroup?: string;
        validationError?: string;
    }
}

const ValidationErrors: React.FC<ValidationErrorsProps> = ({validationErrors, className}) => {
    return (
        <div className={className?.validationErrorsGroup}>
            {validationErrors.map(error => <div key={error} className={className?.validationError}>{error}</div>)}
        </div>
    )
}

export default ValidationErrors;
